package entidades;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import entidades.BasePage;;

public class Usuario extends BasePage {
	
	public static final String INPUT_USERNAME ="user_login";
	public static final String INPUT_EMAIL = "email";
	public static final String INPUT_FIRSTNAME = "first_name";
	public static final String INPUT_LASTNAME = "last_name";
	public static final String INPUT_URL = "url";
	public static final String BUTTON_LOGIN = "createusersub";

	public Usuario(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(id=INPUT_USERNAME)
	private WebElement inputUserName;
	
	@FindBy(id=INPUT_EMAIL)
	private WebElement inputEmail;

	@FindBy(id=INPUT_FIRSTNAME)
	private WebElement inputFirstName;
	
	@FindBy(id=INPUT_LASTNAME)
	private WebElement inputLastName;
	
	@FindBy(id=INPUT_URL)
	private WebElement inputUrl;
	
	@FindBy(id=BUTTON_LOGIN)
	private WebElement buttonLogIn;
	
	public void CrearUsuario() throws InterruptedException {
		inputUserName.sendKeys("gaston");
		inputEmail.sendKeys("gaston@gmail.com");
		inputFirstName.sendKeys("Gaston");
		inputLastName.sendKeys("Alonso");
		inputUrl.sendKeys("www.algunaURL.com");
		buttonLogIn.click();
	}
}
