package hola.Examen;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.ChromeDriverManager;

import entidades.Usuario;

public class AdminTest {
	
	WebDriver driver;
	
	@Test
	public void InicioSesionYCreacionDeUsuario() throws InterruptedException {
		
		//Hago el Login con las credenciales dadas
		
	    WebElement txtUN = driver.findElement(By.id("user_login"));
		WebElement txtPass = driver.findElement(By.id("user_pass"));	
		WebElement btnSubmit = driver.findElement(By.id("wp-submit"));		
		
		txtUN.sendKeys("user");
		txtPass.sendKeys("3uYzlwkY1nME");
		btnSubmit.click();
		
		//Click en USERS
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement claseUsuario = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"menu-users\"]/a/div[2]")));	
		claseUsuario.click();
		
		
		//Click en ADD NEW y chequeo del texto "Add New User" 
		WebElement bottonAddnew = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("page-title-action")));
		bottonAddnew.click();
			
		WebElement texto = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("add-new-user")));
		Assert.assertEquals(texto.getText(), "Add New User"); 
		
		
		//Registro un nuevo usuario con la page Usuario
        Usuario usuario = new Usuario (driver);
        usuario.CrearUsuario();
	}
	
	@BeforeTest
	public void InicializarDriver() throws InterruptedException {
		ChromeDriverManager.getInstance().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://54.212.7.174/admin");
	}
	
	@AfterTest
	public void CerrarDriver() throws InterruptedException {
		driver.close();
	}

}
